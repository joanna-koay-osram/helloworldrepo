// Note that you must leave a blank line between the copyright header and your
// actual doc string, to avoid having this documentation shown in godoc.

// helloworldrepo is a repo demonstrating Go documentation.
//
// Introduction
//
// Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
// Suspendisse sit amet odio at quam fringilla convallis quis non libero. 
// In non luctus est, sed tristique tellus. 
//
// Description
//
// Phasellus dignissim justo id elementum sollicitudin. 
// Pellentesque id iaculis ligula. Maecenas porta 
// nisi ut bibendum gravida. Etiam eget enim eget augue rutrum tempus. 
// Maecenas viverra ipsum quis dapibus pharetra. Integer imperdiet neque 
// sit amet lacus vehicula viverra.
package main
