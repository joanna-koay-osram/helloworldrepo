package sub

// Constants are sorted to the top of the code documentation.
//
// Apple
//
// Because it is declared on its own and not in a const() block, AppleName gets
// full formatting support.
const AppleName = "apple"