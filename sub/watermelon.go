package sub

// Constants are sorted to the top of the code documentation.
//
// Watermelon
//
// Because it is declared on its own and not in a const() block, WatermelonName gets
// full formatting support.
const WatermelonName = "Watermelon"