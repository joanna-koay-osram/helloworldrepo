package sub

// Constants are sorted to the top of the code documentation.
//
// Orange
//
// Because it is declared on its own and not in a const() block, OrangeName gets
// full formatting support.
const OrangeName = "Orange"